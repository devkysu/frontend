import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  baseUrl: '';
  public get(path: string, params?: any): Observable<any> {
    return this.http.get(path)
  }

  public post() {

  }
}
