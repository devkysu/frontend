import { Injectable } from '@angular/core';
import { Observable , of as observableOf} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChangeBackgroundService {

  constructor() { }

  public onChangeBg(isDark){
    localStorage.setItem('isDark', isDark)
  }

  public isDarkBg(): boolean {
    let isDark = localStorage.getItem('isDark')
    return isDark == 'true'
  }



}
