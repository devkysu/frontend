import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './layouts/shares/header/header.component';
import { AppRoutingModule } from './app-routing.module';
import { FooterComponent } from './layouts/shares/footer/footer.component';
import { SidebarRightComponent } from './layouts/shares/sidebar-right/sidebar-right.component';
import { SearchModule } from './modules/search/search.module';
import { SearchComponent } from './modules/search/search.component';
import { PagesModule } from './pages/pages.module';
import { ChangeBackgroundService } from './core/change-background.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SidebarRightComponent,
  ],
  exports: [],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SearchModule,
    PagesModule,
    HttpClientModule
  ],
  providers: [ChangeBackgroundService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
