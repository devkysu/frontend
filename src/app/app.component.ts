import { Component, Renderer2 } from '@angular/core';
import { ChangeBackgroundService } from './core/change-background.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'frontend';
  classBg = 'bg-dark'
  constructor(private changeBgService: ChangeBackgroundService, private renderer: Renderer2) {
    
   
  }
}
