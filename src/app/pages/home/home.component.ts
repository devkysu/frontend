import { Component, OnInit } from '@angular/core';
import { PostService } from 'src/app/services/post.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private postService: PostService) { }
  posts
  xxx: string
  ngOnInit(): void {
    this.postService.getPosts().subscribe(data => {
      this.posts = data
    });
  }

}
