import { Injectable } from '@angular/core';
import { ApiService } from '../core/api.service';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private apiService: ApiService) { }

  public getPosts(){
    let url = 'https://jsonplaceholder.typicode.com/posts'
    return this.apiService.get(url)
  }
}
