import { Injectable } from '@angular/core';
import { ApiService } from '../core/api.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private apiService: ApiService) { }

  public getUser() {
    let url = 'https://jsonplaceholder.typicode.com/users'
    let promise = this.apiService.get(url).subscribe(data => {
      console.log(data);
      
    })
  }
}
