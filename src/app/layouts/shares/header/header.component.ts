import { Component, OnInit, Renderer2, OnDestroy } from '@angular/core';
import { ChangeBackgroundService } from 'src/app/core/change-background.service';
import { isBoolean } from 'util';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  isDark: boolean = true;
  constructor(private changeBgService: ChangeBackgroundService, private renderer: Renderer2) { }
  isAva: boolean
  ngOnInit(): void {
    this.isAva = this.changeBgService.isDarkBg();
    this.renderer.addClass(document.body, this.isAva ? 'bg-dark' : 'bg-white')
    this.renderer.removeClass(document.body, this.isAva ? 'bg-white' : 'bg-dark' )
  }

  onChangeBg(){
    this.isDark = !this.isDark;
    this.changeBgService.onChangeBg(this.isDark);
    this.isAva = this.changeBgService.isDarkBg();
    this.renderer.addClass(document.body, this.isAva ? 'bg-dark' : 'bg-white')
    this.renderer.removeClass(document.body, this.isAva ? 'bg-white' : 'bg-dark' )
  }

  ngOnDestroy(){
    
  }
  

}
